package university_;


import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestListDemo {


    @Test
    public void testGetListSameSurnameWithHenry() {
        Human goodHenry = new Human("Jhonson", "Henry", "Baker", 45);
        List<Human> list = new ArrayList<>();
        List<Human> res = new ArrayList<>();
        list.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        list.add(new Human(goodHenry));
        res.add(new Human(goodHenry));
        list.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        res.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        list.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        res.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        list.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        list.add(new Human("Rainbower", "Zins", "Chovich", 77));
        list.add(new Human("Jhonson", "Reobert", "Grober", 7));
        res.add(new Human("Jhonson", "Reobert", "Grober", 7));
        assertEquals(ListDemo.getListSameSurnameWithHenry(list, goodHenry), res);
    }

    @Test
    public void testDeletHenryFromList() {
        Human goodHenry = new Human("Jhonson", "Henry", "Baker", 45);
        List<Human> list = new ArrayList<>();
        List<Human> res = new ArrayList<>();
        list.add(new Human("Jhonson", "Henry", "Baker", 45));
        list.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        res.add(new Human("Ivanov", "Ivan", "Ivanovich", 66));
        list.add(new Human(goodHenry));
        list.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        res.add(new Human("Jhonson", "Timmy", "Cloud", 25));
        list.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        res.add(new Human("Jhonson", "Boris", "Petrovich", 125));
        list.add(new Human(goodHenry));
        list.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        res.add(new Human("Jhonsons", "Boriz", "Zovich", 87));
        list.add(new Human("Rainbower", "Zins", "Chovich", 77));
        list.add(new Human(goodHenry));
        list.add(new Human(goodHenry));
        res.add(new Human("Rainbower", "Zins", "Chovich", 77));
        list.add(new Human("Jhonson", "Reobert", "Grober", 7));
        list.add(new Human(goodHenry));
        res.add(new Human("Jhonson", "Reobert", "Grober", 7));
        assertEquals(ListDemo.deletHenryFromList(list, goodHenry), res);
        assertEquals(list.get(1), res.get(0));
        list.set(1, new Human(goodHenry));
        assertNotEquals(list.get(1), res.get(0));
    }

    @Test
    public void testRemoveIntersectSets() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        Set<Integer> set1 = new HashSet<>();//-
        Set<Integer> set2 = new HashSet<>();//+
        Set<Integer> set3 = new HashSet<>();//+
        Set<Integer> set4 = new HashSet<>();//-
        Set<Integer> set5 = new HashSet<>();//-
        Set<Integer> set6 = new HashSet<>();//+
        Set<Integer> set7 = new HashSet<>();//+
        set1.add(1);
        set1.add(9);
        set1.add(0);
        set1.add(91);
        set2.add(20);
        set2.add(12);
        set2.add(974);
        set2.add(87);
        set2.add(78);
        set2.add(45);
        set3.add(0);
        set4.add(1);
        set4.add(2);
        set4.add(3);
        set4.add(4);
        set4.add(5);
        set5.add(8);
        set5.add(10);
        set5.add(85);
        set5.add(18);
        set5.add(4);
        set5.add(102);
        set7.add(45);
        set7.add(123);
        set7.add(1234);
        set7.add(12345);
        List<Set<Integer>> list = new ArrayList<>();
        list.add(set1);
        list.add(set2);
        list.add(set3);
        list.add(set4);
        list.add(set5);
        list.add(set6);
        list.add(set7);
        list.add(set4);
        list.add(set6);
        list.add(set7);
        list.add(set2);
        list.add(set4);
        list.add(set6);
        list.add(set7);
        list.add(set3);
        List<Set<Integer>> res = new ArrayList<>();
        res.add(set2);
        res.add(set3);
        res.add(set6);
        res.add(set7);
        res.add(set6);
        res.add(set7);
        res.add(set2);
        res.add(set6);
        res.add(set7);
        res.add(set3);
        assertEquals(ListDemo.removeIntersectSets(list, set), res);
    }

    @Test
    public void testGetSetOfOldest() {
        List<Human> obj = new ArrayList<>();
        Set<Human> res = new HashSet<>();
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Student("Sllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Student("Zllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Human("Kll", "Ads", "Dsa", 12));
        obj.add(new Human("Kll", "Ads", "Dsa", 42));
        obj.add(new Human("Sdll", "Ads", "Dsa", 42));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        obj.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        obj.add(new Human("Qllda", "Xdsds", "Cqadsa", 82));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 42, "Ll"));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 72, "Ll"));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Human("SdKll", "Ads", "Dsa", 12));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "LlD"));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        obj.add(new Human("Adsllda", "aAdsds", "Qqadsa", 82));
        obj.add(new Human("AdsKllda", "Adsds", "Qqadsa", 82));
        obj.add(new Human("dsdKllda", "sAdsds", "Qqadsa", 52));
        obj.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Human("Kllda", "Adsds", "Qqadsa", 82));
        res.add(new Human("Qllda", "Xdsds", "Cqadsa", 82));
        res.add(new Student("Kllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "Ll"));
        res.add(new Student("QqKllda", "Adsds", "Qqadsa", 82, "LlD"));
        res.add(new Human("Adsllda", "aAdsds", "Qqadsa", 82));
        res.add(new Human("AdsKllda", "Adsds", "Qqadsa", 82));
        assertEquals(ListDemo.getSetOfOldest(obj), res);
    }

    @Test
    public void testMakeListFromSet() {
        List<Human> res = new ArrayList<>();
        Set<Human> obj = new HashSet<>();
        obj.add(new Human("Abcd", "Bbc", "Cdd", 18));
        obj.add(new Human("Abcd", "Aac", "Cdd", 78));
        obj.add(new Human("Abcd", "Abc", "Vdd", 178));
        obj.add(new Human("Zebcd", "Abc", "Cdd", 14));
        obj.add(new Human("Abcd", "Dbc", "Cdd", 32));
        obj.add(new Human("Esbcd", "Abc", "Cdd", 8));
        obj.add(new Student("Esbcd", "Abc", "Cdd", 87, "Ff"));
        obj.add(new Student("Asbcd", "Abc", "Cdd", 82, "Fa"));
        obj.add(new Human("Abcd", "Bbc", "Cdd", 7));
        obj.add(new Student("Esbcd", "Abc", "Add", 28, "FE"));

        res.add(new Human("Abcd", "Aac", "Cdd", 78));
        res.add(new Human("Abcd", "Abc", "Vdd", 178));
        res.add(new Human("Abcd", "Bbc", "Cdd", 18));
        res.add(new Human("Abcd", "Bbc", "Cdd", 7));
        res.add(new Human("Abcd", "Dbc", "Cdd", 32));
        res.add(new Student("Asbcd", "Abc", "Cdd", 82, "Fa"));
        res.add(new Student("Esbcd", "Abc", "Add", 28, "FE"));
        res.add(new Human("Esbcd", "Abc", "Cdd", 8));
        res.add(new Student("Esbcd", "Abc", "Cdd", 87, "Ff"));
        res.add(new Human("Zebcd", "Abc", "Cdd", 14));
        assertEquals(ListDemo.makeListFromSet(obj), res);
    }

    @Test
    public void testIdentifeHumans() {
        Map<Integer, Human> mapId = new HashMap<>();
        Set<Integer> setOfId = new HashSet<>();
        Set<Human> res = new HashSet<>();
        mapId.put(1, new Human("Fdd", "Adds", "Acss", 14));
        mapId.put(2, new Human("Edd", "Redsds", "Vbss", 30));
        mapId.put(22, new Human("Zss", "Forsds", "Qrss", 22));
        mapId.put(220, new Human("AsdfZss", "Forsds", "Qrss", 22));
        mapId.put(22475, new Human("QrZss", "GfsdForsds", "SDdQrss", 22));
        mapId.put(25, new Human("Oppsd", "Klos", "Lks", 53));
        mapId.put(117, new Human("Black", "Monds", "Qwre", 1));
        setOfId.add(1);
        setOfId.add(2);
        setOfId.add(22);
        setOfId.add(2774);
        setOfId.add(36);
        setOfId.add(3);
        setOfId.add(25);
        setOfId.add(87);
        setOfId.add(110);
        setOfId.add(117);
        setOfId.add(10);
        setOfId.add(19);
        res.add(new Human("Fdd", "Adds", "Acss", 14));
        res.add(new Human("Edd", "Redsds", "Vbss", 30));
        res.add(new Human("Zss", "Forsds", "Qrss", 22));
        res.add(new Human("Oppsd", "Klos", "Lks", 53));
        res.add(new Human("Black", "Monds", "Qwre", 1));
        ListDemo.identifeHumans(mapId, setOfId);
    }

    @Test
    public void testGetIdAge() {
        Map<Integer, Human> mapId = new TreeMap<>();
        Set<Integer> res = new HashSet<>();
        mapId.put(1, new Human("Fdd", "Adds", "Acss", 14));
        mapId.put(2, new Human("Edd", "Redsds", "Vbss", 30));
        mapId.put(22, new Human("Zss", "Forsds", "Qrss", 22));
        mapId.put(220, new Human("AsdfZss", "Forsds", "Qrss", 22));
        mapId.put(22475, new Human("QrZss", "GfsdForsds", "SDdQrss", 17));
        mapId.put(25, new Human("Oppsd", "Klos", "Lks", 53));
        mapId.put(117, new Human("Black", "Monds", "Qwre", 1));
        mapId.put(2117, new Human("Redflack", "Monds", "Qwre", 18));
        res.add(2);
        res.add(22);
        res.add(220);
        res.add(25);
        res.add(2117);
        assertEquals(ListDemo.getIdAge(mapId), res);
    }

    @Test
    public void testGetMapId() {
        Map<Integer, Human> mapId = new HashMap<>();
        Map<Integer, Integer> res = new HashMap<>();
        mapId.put(1, new Human("Fdd", "Adds", "Acss", 14));
        mapId.put(2, new Human("Edd", "Redsds", "Vbss", 30));
        mapId.put(22, new Human("Zss", "Forsds", "Qrss", 22));
        mapId.put(220, new Human("AsdfZss", "Forsds", "Qrss", 22));
        res.put(1, 14);
        res.put(2, 30);
        res.put(22, 22);
        res.put(220, 22);
        assertEquals(res, ListDemo.getMapId(mapId));


    }

    @Test
    public void testGetAgeMap() {
        Set<Human> obj = new HashSet<>();
        Map<Integer, List<Human>> res = new HashMap<>();
        List<Human> list=new ArrayList<>();
        List<Human> list1=new ArrayList<>();
        List<Human> list2=new ArrayList<>();
        List<Human> list3=new ArrayList<>();
        List<Human> list4=new ArrayList<>();
        list.add(new Human("Abcd", "Bbc", "Cdd", 18));
        res.put(18,list);
        list1.add(new Human("Ad", "Ac", "Cd", 78));
        list1.add(new Human("Abcd", "Aac", "Cdd", 78));
        res.put(78,list1);
        list2.add(new Human("Abcd", "Abc", "Vdd", 178));
        res.put(178,list2);
        list3.add(new Human("Zebcd", "Abc", "Cdd", 14));
        res.put(14,list3);
        list4.add(new Human("Abcd", "Dbc", "Cdd", 32));
        res.put(32,list4);
        obj.add(new Human("Abcd", "Bbc", "Cdd", 18));
        obj.add(new Human("Ad", "Ac", "Cd", 78));
        obj.add(new Human("Abcd", "Aac", "Cdd", 78));
        obj.add(new Human("Abcd", "Abc", "Vdd", 178));
        obj.add(new Human("Zebcd", "Abc", "Cdd", 14));
        obj.add(new Human("Abcd", "Dbc", "Cdd", 32));

       assertEquals(res, ListDemo.getAgeMap(obj));

    }


}
